Run `npx nx g @nrwl/react:app my-app` to generate an application.

## Generate a library

Run `npx nx g @nrwl/react:lib my-lib --js=true --unitTestRunner=none` to generate a library.
> npx nx g @nrwl/react:lib fe-core --js=true --unitTestRunner=none

## Development server

Run `npx nx serve my-app` for a dev server. Navigate to http://localhost:3000/. The app will automatically reload if you change any of the source files.

## Generate a component

Run `npx nx g @nrwl/react:component my-component --project=my-app --js=true --directory=my-path --skipTests=true` to generate a new component.
> npx nx g @nrwl/react:component Column --project=trello-app --js=true --directory=app/components --skipTests=true

## Build

Run `npx nx build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Understand your workspace

Run `npx nx graph` to see a diagram of the dependencies of your projects.
