module.exports = {
    ...require("prettier-airbnb-config"),
    "bracketSpacing": true,
    "semi": true,
    "singleQuote": true,
    "arrowParens": "always",
    // "trailingComma": "all",
}
