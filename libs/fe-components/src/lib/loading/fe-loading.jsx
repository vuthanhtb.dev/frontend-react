import React from 'react';
import ReactDOM from 'react-dom';

import './_fe-loading.scss';

function FeLoading() {
  const gridContent = document.querySelector('#root');

  const loadingPanel = (
    <div className="lds-ellipsis">
      <div />
      <div />
      <div />
      <div />
    </div>
  );

  return ReactDOM.createPortal(loadingPanel, gridContent);
}

export default FeLoading;
