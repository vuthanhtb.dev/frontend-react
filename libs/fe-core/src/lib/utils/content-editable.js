// onKeyDown
export const saveContentAffterPressEnter = (event) => {
  if (event.key === 'Enter') {
    event.preventDefault();
    event.target.blur();
  }
};

// Selected all input value when click
export const selectAllInlineText = (event) => {
  event.target.focus();
  event.target.select();
};
