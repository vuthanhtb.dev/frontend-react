import React from 'react';
import './card.scss';

export function Card(props) {
  const { card: { title, cover } } = props;

  return (
    <div className="card-item">
      {cover && <img src={cover} alt="title" draggable="false" />}
      {title}
    </div>
  );
}

export default Card;
