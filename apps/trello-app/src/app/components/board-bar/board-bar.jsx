import React from 'react';

import './board-bar.scss';

export function BoardBar(props) {
  return (
    <nav className="navbar-board">Board Bar</nav>
  );
}

export default BoardBar;
