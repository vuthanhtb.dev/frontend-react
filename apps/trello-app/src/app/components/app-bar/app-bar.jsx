import React from 'react';

import './app-bar.scss';

export function AppBar() {
  return (
    <nav className="navbar-app">
      <h1>Welcome to AppBar!</h1>
    </nav>
  );
}

export default AppBar;
