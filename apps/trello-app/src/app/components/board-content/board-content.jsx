import React, { useEffect, useState } from 'react';
import { Container, Draggable } from 'react-smooth-dnd';
import {
  Container as BootstrapContainer,
  Row,
  Col,
  Form,
  Button,
} from 'react-bootstrap';
import { isEmpty } from 'lodash';

import { mapOrder, applyDrag } from '@frontend-react/fe-core';
import Column from '@trello-components/column/column';
import { initialData } from '@trello-app/app/actions/initialData';

import './board-content.scss';

export function BoardContent() {
  const newColumnInputRef = React.useRef(null);

  const [board, setBoard] = useState({});
  const [columns, setColumns] = useState([]);
  const [isOpen, setOpen] = useState(false);
  const [newTitleColumn, setNewTitleColumn] = useState('');

  useEffect(() => {
    const boardsFromDB = initialData.borads.find(
      (item) => item.id === 'borad-1'
    );
    if (boardsFromDB) {
      setBoard(boardsFromDB);
      setColumns(
        mapOrder(boardsFromDB.columns, boardsFromDB.columnOrder, 'id')
      );
    }
  }, []);

  useEffect(() => {
    if (newColumnInputRef && newColumnInputRef.current) {
      newColumnInputRef.current.focus();
    }
  }, [isOpen]);

  if (isEmpty(board)) {
    return <div className="not-found">Borad not found</div>;
  }

  const onColumnDrop = (dropDragResult) => {
    let newColumns = [...columns];
    newColumns = applyDrag(newColumns, dropDragResult);

    const newBoard = { ...board };
    newBoard.columnOrder = newColumns.map((column) => column.id);
    newBoard.Column = newColumns;

    setColumns(newColumns);
    setBoard(newBoard);
  };

  const onCardDrop = (columnId, dropResult) => {
    if (dropResult.removedIndex !== null || dropResult.addedIndex !== null) {
      const newColumns = [...columns];

      const currentColumn = newColumns.find((column) => column.id === columnId);
      currentColumn.cards = applyDrag(currentColumn.cards, dropResult);
      currentColumn.cardOrder = currentColumn.cards.map((card) => card.id);

      setColumns(newColumns);
    }
  };

  const toggleAddForm = () => {
    setOpen((prev) => !prev);
  };

  const onChange = (event) => {
    event.preventDefault();
    setNewTitleColumn(event.target.value);
  };

  const handleAddingNewColumn = () => {
    if (!newTitleColumn) {
      newColumnInputRef.current.focus();
      return;
    }

    const newColumn = {
      id: Math.random().toString().substring(2, 5),
      boardId: board.id,
      title: newTitleColumn.trim(),
      cardOrder: [],
      cards: []
    };

    const newColumns = [...columns];
    newColumns.push(newColumn);

    const newBoard = { ...board };
    newBoard.columnOrder = newColumns.map((column) => column.id);
    newBoard.Column = newColumns;

    setColumns(newColumns);
    setBoard(newBoard);

    setNewTitleColumn('');
    setOpen(false);
  };

  const onUpdateColumn = (col) => {
    let newCols = [...columns];
    if (col._destroy) {
      newCols = columns.filter((item) => item.id !== col.id);
    } else {
      const colIndexToUpdate = newCols.findIndex((item) => item.id === col.id);
      newCols[colIndexToUpdate].title = col.title;
      newCols.splice(colIndexToUpdate, 1, col);
    }

    const newBoard = { ...board };
    newBoard.columnOrder = newCols.map((column) => column.id);
    newBoard.Column = newCols;

    setBoard(newBoard);
    setColumns(newCols);
  };

  return (
    <div className="board-content">
      <Container
        orientation="horizontal"
        onDrop={onColumnDrop}
        getChildPayload={(index) => columns[index]}
        dragHandleSelector=".column-drag-handle"
        dropPlaceholder={{
          animationDuration: 150,
          showOnTop: true,
          className: 'column-drop-preview',
        }}
      >
        {columns.map((column, index) => (
          <Draggable key={index}>
            <Column
              column={column}
              onCardDrop={onCardDrop}
              onUpdateColumn={onUpdateColumn}
            />
          </Draggable>
        ))}
      </Container>
      <BootstrapContainer className="bootstrap-container">
        {!isOpen && (
          <Row>
            <Col className="add-new-column" onClick={toggleAddForm}>
              <i className="fa fa-plus icon" />
              Add another column
            </Col>
          </Row>
        )}
        {isOpen && (
          <Row>
            <Col className="enter-new-column">
              <Form.Control
                size="sm"
                type="text"
                placeholder="Enter column title"
                className="input-enter-new-column"
                ref={newColumnInputRef}
                value={newTitleColumn}
                onChange={onChange}
                onKeyDown={(event) => event.key === 'Enter' && handleAddingNewColumn()}
              />
              <Button
                variant="success"
                size="sm"
                onClick={handleAddingNewColumn}
              >
                <i className="fa fa-plus icon" />
                Add column
              </Button>
              <span className="cancel-icon" onClick={toggleAddForm}>
                <i className="fa fa-trash icon" />
              </span>
            </Col>
          </Row>
        )}
      </BootstrapContainer>
    </div>
  );
}

export default BoardContent;
