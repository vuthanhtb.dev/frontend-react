import React, { useEffect, useState } from 'react';
import { Container, Draggable } from 'react-smooth-dnd';
import Dropdown from 'react-bootstrap/Dropdown';
import { Form, Button } from 'react-bootstrap';
import { cloneDeep } from 'lodash';

import {
  mapOrder,
  MODAL_ACTION_CONFIRM,
  saveContentAffterPressEnter,
  selectAllInlineText,
} from '@frontend-react/fe-core';
import Card from '@trello-components/card/card';
import { FeConfirmModal } from '@frontend-react/fe-components';

import './column.scss';

export function Column(props) {
  const {
    column: {
      title, cards, cardOrder, id,
    },
    column,
    onCardDrop,
    onUpdateColumn
  } = props;

  const [cardList, setCardList] = useState([]);
  const [columnTitle, setColumnTitle] = useState('');

  const [showConfirmModal, setShowConfirmModal] = useState(false);
  const toggleShowConfirmModal = () => setShowConfirmModal(!showConfirmModal);

  const [isOpenNewCardForm, setOpenNewCardForm] = useState(false);
  const toggleOpenNewCardForm = () => setOpenNewCardForm(!isOpenNewCardForm);

  const [newTitleCard, setNewTitleCard] = useState('');
  const onChange = (event) => setNewTitleCard(event.target.value);

  const newCardInputRef = React.useRef(null);

  useEffect(() => {
    setCardList(cards.length ? mapOrder(cards, cardOrder, 'id') : []);
  }, [cardOrder, cards]);

  useEffect(() => {
    setColumnTitle(title);
  }, [title]);

  useEffect(() => {
    if (newCardInputRef && newCardInputRef.current) {
      newCardInputRef.current.focus();
    }
  }, [isOpenNewCardForm]);

  const onConfirmModalAction = (type) => {
    if (type === MODAL_ACTION_CONFIRM) {
      const newColumn = {
        ...column,
        _destroy: true
      };
      onUpdateColumn(newColumn);
    }
    toggleShowConfirmModal();
  };

  const onChangeTitleColumn = (event) => setColumnTitle(event.target.value);

  const onBlurTitleColumn = (event) => {
    const newColumn = {
      ...column,
      title: event.target.value
    };
    onUpdateColumn(newColumn);
  };

  const handleAddingNewCard = () => {
    if (!newTitleCard) {
      newCardInputRef.current.focus();
      return;
    }

    const newCard = {
      id: Math.random().toString().substring(2, 5),
      boardId: column.boardId,
      columnId: column.id,
      title: newTitleCard.trim(),
      cover: null
    };

    const newColumn = cloneDeep(column);
    newColumn.cards.push(newCard);
    newColumn.cardOrder.push(newCard.id);

    onUpdateColumn(newColumn);
    setNewTitleCard('');
    toggleOpenNewCardForm();
  };

  return (
    <div className="column">
      <header className="column-drag-handle">
        <div className="column-title">
          <Form.Control
            size="sm"
            type="text"
            placeholder="Enter column title"
            className="trello-content-editable"
            spellCheck={false}
            value={columnTitle}
            onChange={onChangeTitleColumn}
            onBlur={onBlurTitleColumn}
            onKeyDown={saveContentAffterPressEnter}
            onMouseDown={(event) => event.preventDefault()}
            onClick={selectAllInlineText}
          />
        </div>
        <div className="column-dropdow-actions">
          <Dropdown>
            <Dropdown.Toggle id="dropdown-basic" className="dropdown-btn" />
            <Dropdown.Menu>
              <Dropdown.Item>Add card...</Dropdown.Item>
              <Dropdown.Item onClick={toggleShowConfirmModal}>Remove column...</Dropdown.Item>
              <Dropdown.Item>Move all cards in this column...</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </header>
      <div className="card-list">
        <Container
          groupName="col"
          onDrop={(dropResult) => onCardDrop(id, dropResult)}
          getChildPayload={(index) => cardList[index]}
          dragClass="card-ghost"
          dropClass="card-ghost-drop"
          dropPlaceholder={{
            animationDuration: 150,
            showOnTop: true,
            className: 'card-drop-preview',
          }}
          dropPlaceholderAnimationDuration={200}
        >
          {
            cardList.map((card, index) => (
              <Draggable key={index}>
                <Card card={card} />
              </Draggable>
            ))
          }
        </Container>
        {
          isOpenNewCardForm && (
            <div className="add-new-card-area">
              <Form.Control
                size="sm"
                as="textarea"
                rows="3"
                placeholder="Enter a title for this card..."
                className="textarea-enter-new-column"
                ref={newCardInputRef}
                value={newTitleCard}
                onChange={onChange}
                onKeyDown={(event) => event.key === 'Enter' && handleAddingNewCard()}
              />
            </div>
          )
        }
      </div>
      <footer>
        {
          isOpenNewCardForm && (
            <div className="add-new-card-actions">
              <Button
                variant="success"
                size="sm"
                onClick={handleAddingNewCard}
              >
                <i className="fa fa-plus icon" />
                Add card
              </Button>
              <span className="cancel-icon" onClick={toggleOpenNewCardForm}>
                <i className="fa fa-trash icon" />
              </span>
            </div>
          )
        }
        {
          !isOpenNewCardForm && (
            <div className="footer-actions" onClick={toggleOpenNewCardForm}>
              <i className="fa fa-plus icon" />
              Add another card
            </div>
          )
        }
      </footer>
      <FeConfirmModal
        show={showConfirmModal}
        onAction={onConfirmModalAction}
        title="Remove column"
        content={`Are you sure you want to remove <strong>${title}</strong>.<br /> All related cards will also be removed!`}
      />
    </div>
  );
}

export default Column;
