import React from 'react';

import AppBar from '@trello-components/app-bar/app-bar';
import BoardBar from '@trello-components/board-bar/board-bar';
import BoardContent from '@trello-components/board-content/board-content';

import './app.scss';

export function App() {
  return (
    <div className="app-container">
      <AppBar />
      <BoardBar />
      <BoardContent />
    </div>
  );
}

export default App;
