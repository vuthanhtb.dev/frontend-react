export const initialData = {
  borads: [
    {
      id: 'borad-1',
      columnOrder: ['column-1', 'column-2', 'column-3'],
      columns: [
        {
          id: 'column-1',
          boradId: 'borad-1',
          title: 'To do column',
          cardOrder: ['card-1', 'card-2', 'card-3', 'card-4', 'card-5', 'card-6', 'card-7'],
          cards: [
            {
              id: 'card-1', boradId: 'borad-1', columnId: 'column-1', title: 'title of card-1', cover: 'https://haycafe.vn/wp-content/uploads/2022/02/Anh-gai-xinh-Viet-Nam.jpg',
            },
            {
              id: 'card-2', boradId: 'borad-1', columnId: 'column-1', title: 'title of card-2', cover: null,
            },
            {
              id: 'card-3', boradId: 'borad-1', columnId: 'column-1', title: 'title of card-3', cover: null,
            },
            {
              id: 'card-4', boradId: 'borad-1', columnId: 'column-1', title: 'title of card-4', cover: null,
            },
            {
              id: 'card-5', boradId: 'borad-1', columnId: 'column-1', title: 'title of card-5', cover: null,
            },
            {
              id: 'card-6', boradId: 'borad-1', columnId: 'column-1', title: 'title of card-6', cover: null,
            },
            {
              id: 'card-7', boradId: 'borad-1', columnId: 'column-1', title: 'title of card-7', cover: null,
            },
          ],
        },
        {
          id: 'column-2',
          boradId: 'borad-1',
          title: 'Inprogress column',
          cardOrder: ['card-10', 'card-9', 'card-8'],
          cards: [
            {
              id: 'card-8', boradId: 'borad-1', columnId: 'column-2', title: 'title of card-8', cover: null,
            },
            {
              id: 'card-9', boradId: 'borad-1', columnId: 'column-2', title: 'title of card-9', cover: null,
            },
            {
              id: 'card-10', boradId: 'borad-1', columnId: 'column-2', title: 'title of card-10', cover: null,
            },
          ],
        },
        {
          id: 'column-3',
          boradId: 'borad-1',
          title: 'Done column',
          cardOrder: ['card-11', 'card-12', 'card-13'],
          cards: [
            {
              id: 'card-11', boradId: 'borad-1', columnId: 'column-3', title: 'title of card-11', cover: null,
            },
            {
              id: 'card-12', boradId: 'borad-1', columnId: 'column-3', title: 'title of card-12', cover: null,
            },
            {
              id: 'card-13', boradId: 'borad-1', columnId: 'column-3', title: 'title of card-13', cover: null,
            },
          ],
        },
      ],
    },
  ],
};
